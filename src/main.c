#include <stdio.h>
#include <stdlib.h>

#include "vector.h"


#define TAMANO_TEXTO 64

#define PRUEBA_INTERACTIVA 1
#define PRUEBA_REGULAR 2
#define PRUEBA_ORTOGONAL 3


int es_doble(char *texto);


int main()
{
	char ingreso[TAMANO_TEXTO];

	int tipo_de_prueba = PRUEBA_INTERACTIVA;

	vector3D vector1;
	vector3D vector2;

	double producto_punto = 0;
	vector3D producto_cruz;
	double magnitud_v1 = 0;
	double magnitud_v2 = 0;
	int validar_ortogonalidad = 0;

	printf("Prueba de la libreria \'libvector\'\n\n");

	switch(tipo_de_prueba)
	{
		case PRUEBA_ORTOGONAL:
			printf("PRUEBA ORTOGONAL...\n\n");
			vector1.x = (float)6.000000;
			vector1.y = (float)-1.000000;
			vector1.z = (float)2.000000;
			vector2.x = (float)2.000000;
			vector2.y = (float)-2.000000;
			vector2.z = (float)-7.000000;
			break;

		case PRUEBA_REGULAR:
			printf("PRUEBA REGULAR...\n\n");
			vector1.x = (float)3.000000;
			vector1.y = (float)4.000000;
			vector1.z = (float)5.000000;
			vector2.x = (float)4.000000;
			vector2.y = (float)3.000000;
			vector2.z = (float)5.000000;
			break;

		default:
			printf("Ingrese el vector 1:\n");

			do
			{
				printf("\tCoordenada x: ");
				scanf("%s", ingreso);
			}while(!es_doble(ingreso));
			sscanf(ingreso, "%f", &vector1.x);

			do
			{
				printf("\tCoordenada y: ");
				scanf("%s", ingreso);
			}while(!es_doble(ingreso));
			sscanf(ingreso, "%f", &vector1.y);

			do
			{
				printf("\tCoordenada z: ");
				scanf("%s", ingreso);
			}while(!es_doble(ingreso));
			sscanf(ingreso, "%f", &vector1.z);

			printf("Ingrese el vector 2:\n");

			do
			{
				printf("\tCoordenada x: ");
				scanf("%s", ingreso);
			}while(!es_doble(ingreso));
			sscanf(ingreso, "%f", &vector2.x);

			do
			{
				printf("\tCoordenada y: ");
				scanf("%s", ingreso);
			}while(!es_doble(ingreso));
			sscanf(ingreso, "%f", &vector2.y);

			do
			{
				printf("\tCoordenada z: ");
				scanf("%s", ingreso);
			}while(!es_doble(ingreso));
			sscanf(ingreso, "%f", &vector2.z);
	}

	printf("\nEl vector1 es: (%f, %f, %f)\n", vector1.x, vector1.y, vector1.z);
	printf("El vector2 es: (%f, %f, %f)\n\n", vector2.x, vector2.y, vector2.z);

	producto_punto = dotproduct(vector1, vector2);
	producto_cruz = crossproduct(vector1, vector2);
	magnitud_v1 = magnitud(vector1);
	magnitud_v2 = magnitud(vector2);
	validar_ortogonalidad = esOrtogonal(vector1, vector2);

	printf("vector1 . vector2 = %f\n", producto_punto);
	printf("vector1 x vector2 = (%f, %f, %f)\n", producto_cruz.x, producto_cruz.y, producto_cruz.z);
	printf("La magnitud del vector 1 es: %f\n", magnitud_v1);
	printf("La magnitud del vector 2 es: %f\n", magnitud_v2);
	printf("Los vectores ");
	if(validar_ortogonalidad)
	{
		printf("SI");
	}
	else
	{
		printf("NO");
	}
	printf(" son ortogonales.\n");

}


int es_doble(char *texto)
{
	int i;
	char c;
	int salida;
	int puntos = 0;

	salida = 1;

	for(i=0; i<TAMANO_TEXTO; i++)
	{
		c = texto[i];

		if(i==0 && c==0)
		{
			return 0;
			break;
		}

		else if(c==0 || texto[i]==0)
		{
			break;
		}

		switch(c)
		{
            case 45:
                if(i>0) return 0;
                break;
            case 46:
                puntos++;
                if(puntos>1) return 0;
                break;
            default:
                if(!(48<=c && c<=57)) return 0;
		}
	}
	return salida;
}
