#include <math.h>
#include "vector.h"

/* implementar aquí las funciones requeridas */

float dotproduct(vector3D v1, vector3D v2)
{
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

vector3D crossproduct(vector3D v1, vector3D v2)
{
	vector3D v3;

	v3.x = v1.y*v2.z - v1.z*v2.y;
	v3.y = v1.z*v2.x - v1.x*v2.z;
	v3.z = v1.x*v2.y - v1.y*v2.x;

	return v3;
}

float magnitud(vector3D v)
{
	return sqrtf( v.x * v.x + v.y * v.y + v.z * v.z);
}

int esOrtogonal(vector3D v1, vector3D v2)
{
	float cero = (float)0.000000;
	float precision = (float)0.0001;
	float producto_punto;

	producto_punto = dotproduct(v1,v2);

	if (((producto_punto - precision) < cero) && ((producto_punto + precision) > cero))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

